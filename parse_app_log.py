#!/usr/bin/python

from sys import argv
from datetime import datetime
import glob
import argparse
import os.path


def get_args():
    parser = argparse.ArgumentParser(description='Parse app.log produced by autotesting and get CLI response times',
                                     epilog='Example: ./parse_response_times.py -dir /mnt/autotest/logs/auto '
                                            '--platform ig-10')
    parser.add_argument('-dir', type=str,
                        help='directory with autotesting logs', required=True)
    parser.add_argument('-p', '--platform', type=str,
                        help='get times for specified PLATFORM only (e.g. hw-5000, hw-50-n2, ig-10)', default='')
    parser.add_argument('-day', type=str,
                        help='find logs for specified DAY (e.g. 30_05), default: today',
                        default=datetime.now().strftime("%d_%m"))
    args = parser.parse_args()
    return vars(args)


def get_timestamp(logline):
    """extract timestamp from log line and return datetime object"""
    dt = logline[11:23].decode(encoding='iso-8859-1')
    try:
        dt_obj = datetime.strptime(dt, '%H:%M:%S.%f')
        return dt_obj
    except ValueError:
        return 0


def print_results(results):
    print(sorted(results.items()))


if __name__ == "__main__":
    params = get_args()

    path_to_logs = os.path.join(params['dir'], params['day']+'*'+params['platform']+'*', "applicationLogs/app.log")
    applogs = glob.glob(path_to_logs)
    print(applogs)

    junk_length = 5
    timestamp_start, timestamp_end = 0, 0

    for applog in applogs:
        with open(applog, mode='rb') as log:
            times = {}
            # find first hwServer#
            line = next(log)
            while b'String hwServer# was successfully found.' not in line:
                line = next(log)

            # now we can start searching for next command
            for line in log:
                if b'main:send:42' not in line:
                    if b'DEBUG' in line:
                        timestamp_start = get_timestamp(line)
                    continue
                else:
                    command = line[51:]
                    # filtering some junk
                    if len(command) < junk_length or b'q was successfully send' in command \
                        or b'21 was successfully send' in command \
                        or b'24 was successfully send' in command:
                        continue
                    try:
                        cur_line = next(log)
                        # now we're waiting for lines:
                        # String <...> was successfully found
                        # String <...> was found
                        while (b'String' and b'was' and b'found') not in cur_line:
                            if b'DEBUG' in cur_line:
                                timestamp_end = get_timestamp(cur_line)
                            cur_line = next(log)
                        prepared_command = command.decode(encoding='iso-8859-1').rstrip('\n')
                        td = timestamp_end - timestamp_start
                        times[prepared_command] = td.total_seconds()
                        print(timestamp_start, timestamp_end, td, prepared_command)
                    except StopIteration:
                        break

            print_results(times)
