#!/usr/bin/env python3.4

import argparse
import csv
import time
import serial

from datetime import datetime

from SiesPacketGen import SiesPacketGen


def parse_args():
    """ parse arguments and returns dictionary with script parameters"""
    parser = argparse.ArgumentParser(description='Pass command into SIES and measure response time')

    parser.add_argument(
        '-o', type=str, dest='csv', metavar='file_name.csv',
        help='output CSV file',
        required=True)

    parser.add_argument('-t', type=float, dest='iter_sleep',
        help='time between iterations (sec), default: 0',
        default=0, required=False)

    parser.add_argument('-n', type=int, dest='iter_count',
        help='number of iterations, default: 5',
        default=5, required=False)

    parser.add_argument('-p', '-port', type=str, dest='port',
        help='SIES port, default: /dev/ttyACMO',
        default='/dev/ttyACM0', required=False)

    parser.add_argument('-l', type=int, dest='payload_length',
        help='payload length in bytes, default: 100',
        default=100, required=False)

    parser.add_argument('-b', type=int, dest='baudrate',
                        help='baudrate, default: 115200',
                        choices=[38400, 115200],
                        default=115200, required=False)

    parser.add_argument('-d', '--debug',
                        help='print SIES packets to stdout', action='store_true',
                        default=False)

    args = parser.parse_args()
    return vars(args)


def execute_command(console, send, expect=b'\x00\x00\x00\x00', timeout=0.5):
    """ Send command to console port and wait for answer.

    Args:
        console - PySerial object
        send - data to send into console port
        expect - what we will wait in answer, by default 0 as success return code
        timeout (sec) - how long we will wait for an answer

    SIES response looks like:
    b'\x13\x00\x00\x00\x00\x00\x7fT\xe0\x932(\xf4C\xed2%Z\xd5'
    0-2 bytes mean full packet size:
    e.g. \x13\x00 = 19 bytes
    """
    data, chunk = b'', b''
    response_time, remaining_data = 0, 0
    is_success, length_determined = False, False

    FULL_LENGTH_IN_BYTES = 2

    console.reset_input_buffer()
    console.reset_output_buffer()

    start_time = time.time()

    console.write(send)
    console.flush()
    send_time = time.time() - start_time

    while time.time() - start_time < timeout:
        if not length_determined:
            # read FULL_PACKET_SIZE bytes to get length of response
            if console.in_waiting >= FULL_LENGTH_IN_BYTES:
                data = ser.read(FULL_LENGTH_IN_BYTES)
                remaining_data = int.from_bytes(data, byteorder='little')-FULL_LENGTH_IN_BYTES
                length_determined = True
        else:
            # read remaining data without first 2 bytes
            if console.in_waiting == remaining_data:
                chunk = console.read(remaining_data)
                response_time = time.time() - start_time
                data += chunk
                break

    if expect in data:
        is_success = True

    return [round(response_time, 5), int(is_success), data]


def prepare_for_excel(line):
    """take list and replace dots with commas"""
    ready_for_excel = []
    for index, value in enumerate(line):
        ready = str(line[index]).replace('.', ',')
        ready_for_excel.append(ready)
    return ready_for_excel


def write_to_csv(csv_name, data, mode='a', newline=''):
    """write to csv file in specified mode"""
    with open(csv_name, mode=mode, newline=newline) as f:
        writer = csv.writer(f, delimiter=';', dialect='excel')
        writer.writerow(data)


if __name__ == "__main__":

    params = parse_args()
    csv_raw = params['csv']
    csv_average = csv_raw + '.average'

    average = dict(echo=0, enc=0, dec=0, mac_gen=0, mac_check=0, hash=0)

    try:
        ser = serial.Serial(
            port=params['port'],
            baudrate=params['baudrate'],
            parity=serial.PARITY_EVEN,
            timeout=0
        )

    except serial.serialutil.SerialException as e:
        print(e)
        exit(1)

    # add csv header
    csv_header = ['datetime',
                  'ECHO_time', 'ECHO_success',
                  'ENC_time', 'ENC_success',
                  'DEC_time', 'DEC_success',
                  'MAC_GEN_time', 'MAC_GEN_success',
                  'MAC_CHECK_time', 'MAC_CHECK_success',
                  'HASH_time', 'HASH_success',
    ]
    write_to_csv(csv_raw, csv_header, mode='w')

    iter_count = params['iter_count']
    iter_sleep = params['iter_sleep']
    debug = params['debug']

    sies_packet_gen = SiesPacketGen(params['payload_length'])

    for i in range(iter_count):

        iter_start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S,%f')[:-3]

        # ========================= ECHO ===========================

        data_for_sies = sies_packet_gen.generate('ECHO')
        if debug:
            print('Echo send:', data_for_sies)
        echo_out = execute_command(ser, data_for_sies['command'])
        average['echo'] += echo_out[0]
        if debug:
            print('Echo answer:', echo_out)

        # ========================= CRYPT/DECRYPT ===========================

        data_for_sies = sies_packet_gen.generate('ENC')
        if debug:
            print('Encrypt send:', data_for_sies)
        payload_sent = data_for_sies['payload']

        encrypt_out = execute_command(ser, data_for_sies['command'])
        average['enc'] += encrypt_out[0]
        if debug:
            print('Encrypt answer:', encrypt_out)

        # send encrypted data back to SIES
        data_for_sies = sies_packet_gen.generate_from(encrypt_out[2], 'DEC')
        if debug:
            print('Decrypt send:', data_for_sies)
        decrypt_out = execute_command(ser, data_for_sies['command'])
        average['dec'] += decrypt_out[0]
        if debug:
            print('Decrypt answer:', decrypt_out)
        payload_received = decrypt_out[2][6:]

        if payload_sent != payload_received:
            print(iter_start_time, 'Encrypt\decrypt FAIL. Payloads are not equal!')

        # ========================= MAC_GEN/MAC_CHECK ===========================

        data_for_sies = sies_packet_gen.generate('MAC_GEN')
        if debug:
            print('MAC gen send:', data_for_sies)
        payload_sent = data_for_sies['payload']

        mac_gen_out = execute_command(ser, data_for_sies['command'])
        average['mac_gen'] += mac_gen_out[0]
        if debug:
            print('MAC gen answer:', mac_gen_out)

        # send encrypted data back to SIES
        data_for_sies = sies_packet_gen.generate_from(mac_gen_out[2], 'MAC_CHECK')
        if debug:
            print('MAC check send:', data_for_sies)
        mac_check_out = execute_command(ser, data_for_sies['command'])
        average['mac_check'] += mac_check_out[0]
        if debug:
            print('MAC check answer:', mac_check_out)
        payload_received = mac_check_out[2][6:]

        if payload_sent != payload_received:
            print(iter_start_time, 'MAC gen\check FAIL. Payloads are not equal!')

        # ========================= HASH ===========================

        data_for_sies = sies_packet_gen.generate('HASH')
        if debug:
            print('HASH send:', data_for_sies)
        hash_out = execute_command(ser, data_for_sies['command'])
        average['hash'] += hash_out[0]
        if debug:
            print('HASH answer:', hash_out)

        if debug:
            print('===')

        # CSV line format
        csv_line = [
            iter_start_time,
            echo_out[0], echo_out[1],
            encrypt_out[0], encrypt_out[1],
            decrypt_out[0], decrypt_out[1],
            mac_gen_out[0], mac_gen_out[1],
            mac_check_out[0], mac_check_out[1],
            hash_out[0], hash_out[1],
        ]

        write_to_csv(csv_raw, prepare_for_excel(csv_line))

        time.sleep(iter_sleep)

    # count average and write to separate file
    for key in average:
        average[key] /= iter_count
        average[key] = str(round(average[key]*1000, 5)).replace('.', ',')

    with open(csv_average, mode='w') as f:
        for key, value in average.items():
            line = str(key) + ' ' + str(value) + '\n'
            f.write(line)

    print('Test completed for', params['payload_length'], 'bytes payload.')