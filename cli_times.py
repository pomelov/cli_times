#!/usr/bin/env python

import argparse
import csv
import logging
import time
import serial

from datetime import datetime

from commands import commands


def parse_args():
    """ parse arguments and returns dictionary with script parameters"""
    parser = argparse.ArgumentParser(description='pass command into console port and measure response time')
    parser.add_argument(
        '-i', type=str, dest='input', help='data to send into console port', required=True)
    parser.add_argument(
        '-o', type=str, dest='csv', metavar='file_name.csv', help='output CSV file', required=True)
    parser.add_argument(
        '-t', type=int, dest='iter_sleep', help='time between iterations (sec)', default=10, required=False)
    parser.add_argument(
        '-n', type=int, dest='iter_count', help='number of iterations', default=1, required=False)
    parser.add_argument(
        '-l', '--log', type=str, help='log name', default='comData.log')
    parser.add_argument(
        '-p', '--port', type=str, help='console port', default='/dev/ttyUSB0')
    parser.add_argument(
        '-b', '--baud', type=int, help='baudrate', default=38400, choices=[38400, 115200])
    args = parser.parse_args()
    return vars(args)


def execute_command(console, send, expect, timeout, pull_interval=0.01):
    """ Send command to console port and wait for answer.

    Args:
        console - PySerial object
        send - data to send into console port
        expect - what should port respond after receiving our data
        timeout (sec) - how long we will wait for an answer
        pull_interval (sec) - control response time precision, 0.01 by default
    """
    response_time = 0
    data = b''
    is_success = False

    console.write(send.encode() + b'\n')
    start_time = time.time()

    while time.time() - start_time < timeout:
        bytes_to_read = console.inWaiting()
        if bytes_to_read > 0:
            data += ser.read(bytes_to_read)
            response_time = time.time() - start_time
            if expect.encode() in data or expect.lower().encode() in data:
                is_success = True
                break
        time.sleep(pull_interval)
    return [response_time, int(is_success), data]


def write_to_csv(csv_name, data, mode='a'):
    """write to csv file in specified mode"""
    with open(csv_name, mode=mode) as f:
        writer = csv.writer(f, delimiter=';', dialect='excel')
        writer.writerow(data)


if __name__ == "__main__":

    params = parse_args()

    ser = serial.Serial(
        port=params['port'],
        baudrate=params['baud'],
        timeout=0,
    )

    # add csv header
    out_csv = params['csv']
    csv_header = ['datetime', 'data_to_send', 'response_time (sec)', 'timeout (sec)', 'is_success']
    write_to_csv(out_csv, csv_header, mode='w')

    # define comData logger
    logging.basicConfig(filename=params['log'], level=logging.INFO,
                            format='%(asctime)s [%(levelname)-7s] %(message)s')

    iter_count = params['iter_count']
    iter_sleep = params['iter_sleep']

    for i in range(iter_count):
        for command in commands:
            iter_start_time = datetime.now().strftime('%Y/%m/%d %H:%M:%S,%f')[:-3]
            out = execute_command(ser, command[0], command[1], command[2])

            data_for_csv = [
                iter_start_time,
                command[0],
                str(out[0]).replace('.', ','),
                command[2],
                out[1],
            ]

            # TODO write csv to separate files
            write_to_csv(out_csv, data_for_csv)
            logging.info(out[2])

            time.sleep(iter_sleep)
