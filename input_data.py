HOSTNAME = 'ig10-164b000f#'
input_data = [
    ('mftp stop', HOSTNAME, 20),
    ('mftp start', HOSTNAME, 60),
    ('firewall local show', HOSTNAME, 10),
    ('firewall local add src @any dst @local tcp dport 8080 pass', HOSTNAME, 10),
    ('service ipsec show config', HOSTNAME, 10),
    ('failover show info', HOSTNAME, 10),
    ('firewall local delete 7', '[y/n]', 10),
    ('y', HOSTNAME, 10),
]

keysetup41_left = [
    ('', 'Login:', 5),
    ('user', 'Password:', 5),
    ('user', 'Please select setup wizard operating mode:', 5),
    ('1', '[y/n]', 5),

    # set timezone
    ('y', 'Europe', 5),
    ('8', 'Russia', 5),
    ('39', 'Moscow+00', 5),
    ('2', '1) Yes', 5),
    ('1', 'Or press Enter to leave', 5),

    # would you like to install keys?
    ('', '[t/u/c]', 5),
    ('u', 'and press <Enter>', 5),

    # make sure, that there is only one DST on usb drive
    ('', 'Enter password:', 20),

    # configure network interfaces
    ('11111111', 'Configure interface eth0', 120),
    ('y', 'Use dhcp', 5),
    ('n', 'IP-address', 5),
    ('192.168.39.120', 'netmask', 5),
    ('255.255.0.0', 'Configure interface eth1', 5),
    ('y', 'Use dhcp', 5),
    ('n', 'IP-address', 5),
    ('172.16.0.1', 'netmask', 5),
    ('255.255.0.0', 'Configure interface eth2', 5),

    ('n', 'default gateway', 5),
    ('192.168.195.10', 'DNS server', 5),
    ('n', 'NTP daemon', 5),
    ('n', 'Enter hostname', 5),

    # set hostname
    ('LR-left', 'custom virtual IP', 5),
    ('n', 'probe', 5),
    ('n', 'start VPN services', 5),

    # start VPN
    ('y', 'start the command shell', 5),
    ('y', 'LR-left', 120)
]
