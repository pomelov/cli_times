import os


class SiesPacketGen:
    """\
    Vipnet SIES packet generator for cryptographic operations testing"""

    OPERATIONS = {
        'ENC': b'\x01',
        'DEC': b'\x02',
        'MAC_GEN': b'\x05',
        'MAC_CHECK': b'\x06',
        'HASH': b'\x07',
        'ECHO': b'\x1f',
    }

    def __init__(self, size):
        self.size = size

    def generate(self, operation):
        if operation in self.OPERATIONS:
            op_type = self.OPERATIONS[operation]
        else:
            raise NameError('no such operation in SIES')
        packet_size = (self.size+3).to_bytes(length=2, byteorder='little')
        payload = os.urandom(self.size)
        data_for_sies = {
            'payload': payload,
            'command': op_type + packet_size + payload
        }
        return data_for_sies

    def generate_from(self, encrypted_data, operation):
        """take encrypted answer from SIES, extract content length and payload, repack for sending back"""
        if operation in self.OPERATIONS:
            op_type = self.OPERATIONS[operation]
        else:
            raise NameError('no such operation in SIES')
        packet_size = (self._from_hex(encrypted_data[:2])-6+3).to_bytes(length=2, byteorder='little')
        payload = encrypted_data[6:]
        data_for_sies = {
            'payload': payload,
            'command': op_type + packet_size + payload
        }
        return data_for_sies

    def _from_hex(self, hexed):
        return int.from_bytes(hexed, byteorder='little')
